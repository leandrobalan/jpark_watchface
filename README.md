# Jurassic Park Watchface for the Pebble Time Round #

### How do I get set up? ###

* Clone the repo
* Make sure you have Pebble SDK3 installed
* Run ```pebble build```
* Run ```pebble install --phone <phone IP address>```